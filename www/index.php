<?php

session_start();

require_once "../autoload.php";

use Dashboard\Accounts\Session;
use Dashboard\Http\HttpResponse;
use Dashboard\Router;
use Dashboard\Routes\RouteController;

$response = new HttpResponse();
$session = new Session();

RouteController::init($router = new Router());

$router->dispatch();

?>