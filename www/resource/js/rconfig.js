require.config({
    baseUrl: "/resource/js/lib/",
    paths: {
        "backbone": "../ext/backbone",
        "jquery": "../ext/jquery",
        "underscore": "../ext/underscore"
    },
    shim: {
        "backbone": {
            deps: ['underscore', 'jquery']
        }
    }
});