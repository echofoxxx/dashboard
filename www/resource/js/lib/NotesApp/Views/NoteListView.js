/**
 * Created by echofoxxx on 3/5/15.
 */

define(['backbone', '../Collections/NoteCollection'], function (Backbone, NoteCollection) {

    var NoteListView = Backbone.View.extend({

        constructor: function (notes) {
            this.notes = notes || new NoteCollection();
            Backbone.View.apply(this);
        }

    });

    return NoteListView;

});