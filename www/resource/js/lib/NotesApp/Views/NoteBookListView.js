/**
 * Created by echofoxxx on 3/5/15.
 */

define(['backbone', '../Collections/NoteBookCollection'], function (Backbone, NoteBookCollection) {

    var NoteBookListView = Backbone.View.extend({

        constructor: function (noteBooks) {
            this.noteBooks = noteBooks || new NoteBookCollection();
            Backbone.View.apply(this);
        }

    });

    return NoteBookListView;

});