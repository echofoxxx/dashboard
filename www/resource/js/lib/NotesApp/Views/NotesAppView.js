/**
 * Created by echofoxxx on 3/5/15.
 */

define(['underscore', 'backbone', './NoteBookListView', './NoteListView'], function (_, Backbone, NoteBookListView, NoteListView) {

    var NotesAppView = Backbone.View.extend({

        tagName: 'div',

        initialize: function () {

            //Bind `thisArg`
            _.bindAll(this, 'render');

            //Initialize nested views
            this.noteBookListView = new NoteBookListView(this.model.noteBooks);
            this.noteListView = new NoteListView();

        },

        render: function () {
            Backbone.$(this.noteBookListView.render().el).appendTo(this.el);
            Backbone.$(this.noteListView.render().el).appendTo(this.el);
        }

    });

    return NotesAppView;

});