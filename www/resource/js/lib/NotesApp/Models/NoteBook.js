/**
 * Created by echofoxxx on 2/5/15.
 */

define(['backbone', '../Collections/NoteCollection'], function (Backbone, NoteCollection) {

    var NoteBook = Backbone.Model.extend({

        initialize: function () {

            //Use the provided notes if available, otherwise create a new note collection
            this.notes = this.get('notes') || new NoteCollection();
        }

    });

    return NoteBook;

});