/**
 * Created by echofoxxx on 2/5/15.
 */

define(['backbone', '../Collections/ColorTagCollection'], function (Backbone, ColorTagCollection) {

    var Note = Backbone.Model.extend({

        initialize: function () {

            //If a tag collection has been provided, use it, otherwise create a new collection
            this.tags = this.get("tags") || new ColorTagCollection();
        }

    });

    return Note;

});