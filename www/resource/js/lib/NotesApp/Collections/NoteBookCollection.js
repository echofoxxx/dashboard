/**
 * Created by echofoxxx on 3/5/15.
 */

define(['backbone', '../Models/NoteBook'], function (Backbone, NoteBook) {

    var NoteBookCollection = Backbone.Collection.extend({
        model: NoteBook
    });

    return NoteBookCollection;

});