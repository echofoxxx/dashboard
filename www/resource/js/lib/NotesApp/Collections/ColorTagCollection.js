/**
 * Created by echofoxxx on 2/5/15.
 */

define(['backbone', '../Models/ColorTag'], function (Backbone, ColorTag) {

    var ColorTagCollection = Backbone.Collection.extend({
        model: ColorTag
    });

    return ColorTagCollection;

});