/**
 * Created by echofoxxx on 2/5/15.
 */

define(['backbone', '../Models/Note'], function (Backbone, Note) {

    var NoteCollection = Backbone.Collection.extend({
        model: Note
    });

    return NoteCollection;

});