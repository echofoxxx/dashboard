<div class="container text-center">
    <div class="row">
        Registrations are currently closed. Sorry.
    </div>
    <br /><br />
    <div class="row">
        <a class="btn btn-default" role="button" href="/">Back to home</a>
    </div>
</div>