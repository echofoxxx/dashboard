<div class="container">
    <div class="row">
        <div class="form-container col-md-8 col-md-offset-2">
            <form class="form-horizontal" action="{$form.action}" method="POST">
                <div class="form-group {if $smarty.get.error && ($smarty.get.error == $errors.invalidEmail || $smarty.get.error == $errors.emailInUse)} has-error {/if}">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" id="email" name="{$form.parameters.email}" class="form-control" {if $smarty.get.fill_email} value="{$smarty.get.fill_email}" {/if} placeholder="Enter your e-mail address" />
                        {if $smarty.get.error == $errors.invalidEmail}
                            <span class="help-block">
                                A valid e-mail address is required for registration
                            </span>
                        {/if}
                        {if $smarty.get.error == $errors.emailInUse}
                            <span class="help-block">
                                This e-mail address is already in use. Please choose another one.
                            </span>
                        {/if}
                    </div>
                </div>
                <div class="form-group {if $smarty.get.error && ($smarty.get.error == $errors.invalidUsername || $smarty.get.error == $errors.usernameInUse)} has-error {/if}">
                    <label for="username" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" id="username" name="{$form.parameters.username}" class="form-control" {if $smarty.get.fill_username} value="{$smarty.get.fill_username}" {/if} placeholder="Enter your chosen username" />
                        {if $smarty.get.error == $errors.usernameInUse}
                            <span class="help-block">
                                This username is already in use. Please choose another one.
                            </span>
                        {else}
                            <span class="help-block">
                                {$username.minLength}-{$username.maxLength} characters, alphabets and numbers only
                            </span>
                        {/if}
                    </div>
                </div>
                <div class="form-group {if $smarty.get.error && $smarty.get.error == $errors.invalidName} has-error {/if}">
                    <label for="name" class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-10">
                        <input type="text" id="name" name="{$form.parameters.name}" class="form-control" {if $smarty.get.fill_name} value="{$smarty.get.fill_name}" {/if} placeholder="Enter full name" />
                        {if $smarty.get.error == $errors.invalidName}
                            <span class="help-block">
                                Your full name is required for registration
                            </span>
                        {/if}
                    </div>
                </div>
                <div class="form-group {if $smarty.get.error && ($smarty.get.error == $errors.invalidPassword) || ($smarty.get.error == $errors.passwordMismatch)} has-error {/if}">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" id="password" name="{$form.parameters.password}" class="form-control" placeholder="Enter your chosen password" />
                        <span class="help-block">
                            Minimum {$password.minLength} characters required
                        </span>
                    </div>
                </div>
                <div class="form-group {if $smarty.get.error && ($smarty.get.error == $errors.invalidConfirmPassword) || ($smarty.get.error == $errors.passwordMismatch)} has-error {/if}">
                    <label for="confirm-password" class="col-sm-2 control-label">Confirm password</label>
                    <div class="col-sm-10">
                        <input type="password" id="confirm-password" name="{$form.parameters.confirmPassword}" class="form-control" placeholder="Re-enter your chosen password" />
                        <span class="help-block">
                            Both passwords must be exactly same
                        </span>
                    </div>
                </div>
                {if $smarty.get.error && $smarty.get.error == $errors.passwordMismatch}
                    <div class="text-center">
                        <h4 class="text-danger">Passwords do not match</h4>
                    </div>
                {/if}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Register</button>
                        <a role="button" href="/login" class="btn btn-default">Already have an account?</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>e