<!DOCTYPE html>
<html>
<head>
    <title>Log in to your dashboard</title>
    <link rel="stylesheet" href="/resource/css/bootstrap.css" />
    <link rel="stylesheet" href="/resource/css/index.css" />
</head>
<body>
<div class="container">
    <div class="col-md-6 col-md-offset-3">
        <div class="form-container">
            <form method="POST" action="{$form.action}">
                <div class="form-group{if $smarty.get.error && ($smarty.get.error == $errors.invalidUsername) || ($smarty.get.error === $errors.invalidCombination)} has-error{/if}">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="{$form.parameters.username}" placeholder="Enter your username" class="form-control" {if $smarty.get.fill_username} value="{$smarty.get.fill_username} {/if}" />
                </div>
                <div class="form-group{if $smarty.get.error && ($smarty.get.error == $errors.invalidPassword) || ($smarty.get.error === $errors.invalidCombination)} has-error{/if}">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="{$form.parameters.password}" placeholder="Enter your password" class="form-control" />
                </div>
                {if $smarty.get.error && $smarty.get.error == $errors.invalidCombination}
                    <div class="text-center">
                        <h4 class="error text-danger">Invalid username/password combination</h4>
                    </div>
                {/if}
                <div class="text-right">
                    <a class="btn btn-default" href="/register" role="button" {if !$registrationOpen}disabled{/if}>Create an account</a>
                    <input type="submit" value="Login" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/resource/js/ext/jquery.js"></script>
<script src="/resource/js/ext/bootstrap.js"></script>
</body>
</html>