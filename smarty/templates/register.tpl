<!DOCTYPE html>
<html>
<head>
    <title>Create a new account</title>
    <link rel="stylesheet" href="/resource/css/bootstrap.css" />
    <link rel="stylesheet" href="/resource/css/index.css" />
</head>
<body>
{if $registrationOpen}
    {include file="partials/register/open.tpl"}
{else}
    {include file="partials/register/closed.tpl"}
{/if}
<script src="/resource/js/ext/jquery.js"></script>
<script src="/resource/js/ext/bootstrap.js"></script>
</body>
</html>