<?php /* Smarty version 3.1.22-dev/29, created on 2015-05-07 16:18:57
         compiled from "/home/echofoxxx/Dashboard/lib/Http/../../smarty/templates/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:828627542554b4319cba455_31874365%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1697ae1f13334f2c00ca0d010fcadf157c35fa1e' => 
    array (
      0 => '/home/echofoxxx/Dashboard/lib/Http/../../smarty/templates/index.tpl',
      1 => 1430995733,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '828627542554b4319cba455_31874365',
  'variables' => 
  array (
    'registrationOpen' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.22-dev/29',
  'unifunc' => 'content_554b4319cc2127_83889672',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_554b4319cc2127_83889672')) {
function content_554b4319cc2127_83889672 ($_smarty_tpl) {
?>
<?php
$_smarty_tpl->properties['nocache_hash'] = '828627542554b4319cba455_31874365';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Log in to your dashboard</title>
    <link rel="stylesheet" href="/resource/css/bootstrap.css" />
    <link rel="stylesheet" href="/resource/css/index.css" />
</head>
<body>
<div class="container">
    <div class="col-md-6 col-md-offset-3">
        <div class="form-container">
            <form method="POST" action="login.php">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="username" placeholder="Enter your username" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" placeholder="Enter your password" class="form-control" />
                </div>
                <div class="text-right">
                    <button class="btn btn-default" <?php if (!$_smarty_tpl->tpl_vars['registrationOpen']->value) {?>disabled<?php }?>>Create an account</button>
                    <input type="submit" value="Login" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
 src="/resource/js/ext/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/resource/js/ext/bootstrap.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
}
?>