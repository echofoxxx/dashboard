<?php

namespace DashboardTest\Scripts;

use Dashboard\Scripts\PostInstall\MySQLSchema;
use Dashboard\Storage\PDOConfigurationFromINI;

/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 6/5/15
 * Time: 4:12 PM
 */

class MySQLSchemaForTest extends MySQLSchema {

    public static function getConfiguration() {
        $config = parent::getConfiguration();
        $mysql = $config->get('mysql');
        $mysql['dbname'] = "dashboard-test";
        $config->set('mysql', $mysql);
        return $config;
    }

    public static function run($event) {
        self::postInstall($event);
    }

}