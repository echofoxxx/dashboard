<?php
namespace DashboardTest\Storage;

use Dashboard\Accounts\User\IdentityCard;
use Dashboard\Accounts\User\Profile;
use Dashboard\Accounts\User\RegistrationForm;
use Dashboard\Storage\Accounts\UserStorage;
use Dashboard\Storage\MySQL\Factory;
use Dashboard\Storage\PDOConfigurationFromINI;

/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 6/5/15
 * Time: 3:46 PM
 */

class UserStorageTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var UserStorage
     */
    private static $storage;

    private static $testAccountDetails = array(
        'username' => 'testing123',
        'email' => 'testing123@testing.me',
        'password' => 'blahblah',
        'name' => 'Abhishek Goyal'
    );

    private static function getConfiguration() {
        return new PDOConfigurationFromINI(__DIR__."/../config.ini");
    }

    private static function getPDO() {
        return (new Factory(self::getConfiguration()))->pdo;
    }

    public static function setUpBeforeClass() {
        self::$storage = new UserStorage(self::getPDO());
    }

    public function testCreateNewUser() {

        //Use a RegistrationForm for testing new account storage
        $form = new RegistrationForm();
        $form->sets(self::$testAccountDetails);

        //Get the ID of the new test user
        $id = self::$storage->create($form);

        fwrite(STDERR, "Created new test user account with the ID ".$id);

        return new IdentityCard(array('id' => $id));

    }

    /**
     * @param IdentityCard $id
     * @depends testCreateNewUser
     * @returns IdentityCard
     */
    public function testCorrectUserDetails($id) {
        $this->assertArraySubset(self::$testAccountDetails, self::$storage->read($id));
        return $id;
    }

    /**
     * @param IdentityCard $id
     * @depends testCorrectUserDetails
     * @return IdentityCard
     */
    public function testUpdateUserDetails($id) {
        self::$storage->update(
            $id,
            (new Profile())->set('email', 'newemail@testing.me')
        );
        self::$testAccountDetails['email'] = 'newemail@testing.me';
        $this->assertArraySubset(self::$testAccountDetails, self::$storage->read($id));
        return $id;
    }

    /**
     * @param IdentityCard $id
     * @depends testUpdateUserDetails
     * @expectedException \Dashboard\Exceptions\Accounts\User\UserDoesNotExist
     * @return IdentityCard mixed
     */
    public function testDeleteTheTestAccount($id) {
        self::$storage->delete($id);
        self::$storage->read($id);
        return $id;
    }

}