<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 12:38 PM
 */

namespace Dashboard;

use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Klein\Klein;

/**
 * Class Router
 * @package Dashboard
 */
class Router {

    /**
     * @var Klein
     */
    private $klein;

    public function __construct() {
        $this->klein = new Klein();
    }

    public function register($method, $path, $onRequest) {
        switch ($method) {
            case Route::METHOD_ALL:
                $this->klein->respond($path, $this->getMiddleCallback($onRequest));
                break;
            default:
                $this->klein->respond($method, $path, $this->getMiddleCallback($onRequest));
        }
    }

    public function dispatch() {
        $this->klein->dispatch();
    }

    private function getMiddleCallback($callback) {
        return function ($request, $response, $service, $app) use ($callback) {
            call_user_func($callback, new HttpRequest(), new HttpResponse(), $app);
        };
    }

}