<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 2:35 PM
 */

namespace Dashboard;


use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;

abstract class Route {

    const METHOD_GET = "GET";
    const METHOD_POST = "POST";
    const METHOD_PUT = "PUT";
    const METHOD_DELETE = "DELETE";
    const METHOD_OPTIONS = "OPTIONS";
    const METHOD_ALL = "ALL";

    abstract public function method();
    abstract public function path();
    abstract public function onRequest(HttpRequest $request, HttpResponse $response, $app);

    public function makeURL($params = array()) {
        return $this->path()."?".http_build_query($params);
    }

}