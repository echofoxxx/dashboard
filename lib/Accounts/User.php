<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 6/5/15
 * Time: 11:17 PM
 */

namespace Dashboard\Accounts;


use Dashboard\Accounts\User\IdentityCard;
use Dashboard\Accounts\User\RegistrationForm;
use Dashboard\Accounts\UserStorage\UserStorageInterface;
use Dashboard\Security\PasswordHasher;
use Dashboard\Util\KeyValueStorage;

class User extends KeyValueStorage {

    /**
     * @var IdentityCard
     */
    private $card;

    /**
     * @var UserStorageInterface
     */
    private $storage;

    public function __construct(IdentityCard $card, UserStorageInterface $storage) {
        $this->storage = $storage;
        $this->card = $card;
        $this->_loadDetails();
    }

    public function verify($password) {
        return PasswordHasher::verify($password, $this->get('password'));
    }

    private function _loadDetails() {
        $this->sets($this->storage->read($this->card));
        return $this;
    }

    public static function create(RegistrationForm $form, UserStorageInterface $userStorage) {
        $form->set('password', PasswordHasher::hash($form->get('password')));
        return $userStorage->create($form);
    }

}