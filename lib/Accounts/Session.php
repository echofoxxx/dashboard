<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 1:04 AM
 */

namespace Dashboard\Accounts;


class Session {

    const KEY_STATUS = "status";
    const KEY_UID = "uid";

    const STATUS_LOGGED_IN = "loggedIn";
    const STATUS_GUEST = "guest";

    public function __construct() {
        $this->initialize();
    }

    private function initialize() {

        //If the status is not STATUS_LOGGED_IN and a KEY_UID exists, remove it
        if($this->status() !== self::STATUS_LOGGED_IN && array_key_exists(self::KEY_UID, $_SESSION)) {
            unset($_SESSION[self::KEY_UID]);
        }

    }

    public function status() {
        return array_key_exists(self::KEY_STATUS, $_SESSION) ? $_SESSION[self::KEY_STATUS] : ($_SESSION[self::KEY_STATUS] = self::STATUS_GUEST);
    }

    public function uid() {
        return array_key_exists(self::KEY_UID, $_SESSION) ? $_SESSION[self::KEY_UID] : null;
    }

    public function loggedIn() {
        return $this->status() === self::STATUS_LOGGED_IN;
    }

    public function logIn(User $user) {
        session_regenerate_id(true);
        $_SESSION[self::KEY_STATUS] = self::STATUS_LOGGED_IN;
        $_SESSION[self::KEY_UID] = $user->get('id');
    }

    public function logOut() {
        session_regenerate_id(true);
        $_SESSION[self::KEY_STATUS] = self::STATUS_GUEST;
    }

}