<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 5:14 PM
 */

namespace Dashboard\Accounts\User;


use Dashboard\Util\KeyValueStorage;

class IdentityCard extends KeyValueStorage {

    public function key() {
        if($this->has('id')) return 'id';
        if($this->has('username')) return 'username';
        if($this->has('email')) return 'email';
        return null;
    }

    public function value() {
        return $this->get($this->key());
    }

}