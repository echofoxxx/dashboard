<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 5:13 PM
 */

namespace Dashboard\Accounts\UserStorage;


use Dashboard\Accounts\User\IdentityCard;
use Dashboard\Accounts\User\Profile;
use Dashboard\Accounts\User\RegistrationForm;

use Dashboard\Exceptions\DatabaseError;
use Dashboard\Exceptions\Accounts\User\UserDoesNotExist;

interface UserStorageInterface {

    /**
     * Create a new user in the database
     * @param RegistrationForm $form
     * @return int The ID of the newly created user
     * @throws DatabaseError When a database error has occurred
     */
    public function create(RegistrationForm $form);

    /**
     * Read the data of an existing user from the database
     * @param IdentityCard $card
     * @return array
     * @throws DatabaseError When a database error has occurred
     * @throws UserDoesNotExist When the requested user does not exist
     */
    public function read(IdentityCard $card);

    /**
     * Update the data of an existing user in the database
     * @param IdentityCard $card
     * @param Profile $profile
     * @return bool
     * @throws DatabaseError When a database error is occurred
     */
    public function update(IdentityCard $card, Profile $profile);

    /**
     * Delete an existing user from the database
     * @param IdentityCard $card
     * @throws DatabaseError
     * @return void
     */
    public function delete(IdentityCard $card);

}