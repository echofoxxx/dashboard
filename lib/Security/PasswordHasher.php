<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 6/5/15
 * Time: 11:19 PM
 */

namespace Dashboard\Security;


class PasswordHasher {

    public static function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public static function verify($password, $hashed_password) {
        return password_verify($password, $hashed_password);
    }

}