<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 2:43 PM
 */

namespace Dashboard\Routes;


use Dashboard\Accounts\Session;
use Dashboard\Controllers\RegistrationController;
use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;

class App extends Route {

    public function method() {
        return self::METHOD_GET;
    }

    public function path() {
        return "/";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {

        $session = new Session();

        if(!$session->loggedIn()) {
            $response->redirect('/login');
            return;
        }

        $response->render('app');

    }

}