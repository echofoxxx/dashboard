<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 10:04 PM
 */

namespace Dashboard\Routes;


use Dashboard\Accounts\Session;
use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;

class Logout extends Route {

    public function method() {
        return self::METHOD_GET;
    }

    public function path() {
        return "/logout";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {
        $session = new Session();
        $session->logOut();
        $response->redirect('/');
    }

}