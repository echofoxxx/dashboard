<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 3:04 PM
 */

namespace Dashboard\Routes\Apps;


use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;

class Notes extends Route {

    public function method() {
        return self::METHOD_GET;
    }

    public function path() {
        return "/notes";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {
        $response->render('apps/notes');
    }

}