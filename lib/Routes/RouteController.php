<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 2:34 PM
 */

namespace Dashboard\Routes;


use Dashboard\Route;
use Dashboard\Router;

//General pages
use Dashboard\Routes\App as AppPage;
use Dashboard\Routes\Login as LoginPage;
use Dashboard\Routes\Register as RegisterPage;
use Dashboard\Routes\Logout as LogoutPage;

//API routes
use Dashboard\Routes\API\Login as LoginAPI;
use Dashboard\Routes\API\Register as RegisterAPI;

//App routes
use Dashboard\Routes\Apps\Notes as NotesAppPage;

class RouteController {

    public static function init(Router $router) {
        self::registerRoutes($router);
    }

    private static function registerRoutes(Router $router) {

        //General
        self::register($router, new AppPage());
        self::register($router, new LoginPage());
        self::register($router, new RegisterPage());
        self::register($router, new LogoutPage());

        //API
        self::register($router, new LoginAPI());
        self::register($router, new RegisterAPI());

        //Apps
        self::register($router, new NotesAppPage());
    }

    private static function register(Router $router, Route $route) {
        $router->register(
            $route->method(),
            $route->path(),
            array($route, 'onRequest')
        );
    }

}