<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 10:04 PM
 */

namespace Dashboard\Routes;


use Dashboard\Accounts\Session;
use Dashboard\Controllers\LoginController;
use Dashboard\Controllers\RegistrationController;
use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;

use Dashboard\Routes\API\Login as LoginAPI;

class Login extends Route {

    const PARAM_ERROR = "error";
    const PARAM_FILL_USERNAME = "fill_username";

    const ERROR_INVALID_USERNAME = "invalid_username";
    const ERROR_INVALID_PASSWORD = "invalid_password";
    const ERROR_INVALID_COMBINATION = "invalid_combination";

    public function method() {
        return self::METHOD_GET;
    }

    public function path() {
        return "/login";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {

        $session = new Session();

        if($session->loggedIn()) {
            $response->redirect('/');
            return;
        }

        $response->render('login', array(
            'registrationOpen' => RegistrationController::$isOpen,
            'form' => array(
                'action' => (new LoginAPI())->path(),
                'parameters' => array(
                    'username' => LoginController::REQUEST_PARAM_USERNAME,
                    'password' => LoginController::REQUEST_PARAM_PASSWORD
                )
            ),
            'errors' => array(
                'invalidUsername' => self::ERROR_INVALID_USERNAME,
                'invalidPassword' => self::ERROR_INVALID_PASSWORD,
                'invalidCombination' => self::ERROR_INVALID_COMBINATION
            )
        ));

    }

}