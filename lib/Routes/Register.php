<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 10:06 PM
 */

namespace Dashboard\Routes;


use Dashboard\Accounts\Session;
use Dashboard\Controllers\RegistrationController;
use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;

use Dashboard\Routes\API\Register as RegisterAPI;

class Register extends Route {

    const PARAM_ERROR = "error";
    const PARAM_FILL_EMAIL = "fill_email";
    const PARAM_FILL_USERNAME = "fill_username";
    const PARAM_FILL_NAME = "fill_name";

    const ERROR_INVALID_EMAIL = "invalid_email";
    const ERROR_INVALID_USERNAME = "invalid_username";
    const ERROR_INVALID_NAME = "invalid_name";
    const ERROR_INVALID_PASSWORD = "invalid_password";
    const ERROR_INVALID_CONFIRM_PASSWORD = "invalid_confirm_password";
    const ERROR_PASSWORDS_DO_NOT_MATCH = "password_mismatch";
    const ERROR_EMAIL_IN_USE = "email_in_use";
    const ERROR_USERNAME_IN_USE = "username_in_use";

    public function method() {
        return self::METHOD_GET;
    }

    public function path() {
        return "/register";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {

        $session = new Session();

        if($session->loggedIn()) {
            $response->redirect('/');
            return;
        }

        $response->render('register', array(
            'registrationOpen' => RegistrationController::$isOpen,
            'form' => array(
                'action' => (new RegisterAPI())->path(),
                'parameters' => array(
                    'email' => RegistrationController::REQUEST_PARAM_EMAIL,
                    'username' => RegistrationController::REQUEST_PARAM_USERNAME,
                    'name' => RegistrationController::REQUEST_PARAM_NAME,
                    'password' => RegistrationController::REQUEST_PARAM_PASSWORD,
                    'confirmPassword' => RegistrationController::REQUEST_PARAM_CONFIRM_PASSWORD
                )
            ),
            'errors' => array(
                'invalidEmail' => self::ERROR_INVALID_EMAIL,
                'invalidUsername' => self::ERROR_INVALID_USERNAME,
                'invalidName' => self::ERROR_INVALID_NAME,
                'invalidPassword' => self::ERROR_INVALID_PASSWORD,
                'invalidConfirmPassword' => self::ERROR_INVALID_CONFIRM_PASSWORD,
                'passwordMismatch' => self::ERROR_PASSWORDS_DO_NOT_MATCH,
                'emailInUse' => self::ERROR_EMAIL_IN_USE,
                'usernameInUse' => self::ERROR_USERNAME_IN_USE
            ),
            'username' => array(
                'minLength' => RegistrationController::USERNAME_MIN_LENGTH,
                'maxLength' => RegistrationController::USERNAME_MAX_LENGTH
            ),
            'password' => array(
                'minLength' => RegistrationController::PASSWORD_MIN_LENGTH
            )
        ));

    }

}