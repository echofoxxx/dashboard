<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 8/5/15
 * Time: 12:16 AM
 */

namespace Dashboard\Routes\API;


use Dashboard\Accounts\Session;
use Dashboard\Accounts\User;
use Dashboard\Controllers\LoginController;
use Dashboard\Exceptions\Accounts\User\UserDoesNotExist;
use Dashboard\Exceptions\Controllers\LoginController\InvalidCombination;
use Dashboard\Exceptions\Controllers\LoginController\InvalidPassword;
use Dashboard\Exceptions\Controllers\LoginController\InvalidUsername;
use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;
use Dashboard\Storage\MySQL\Factory;
use Dashboard\Storage\MySQL\MySQLPDOConfiguration;
use Dashboard\Routes\Login as LoginPage;

class Login extends Route {

    public function method() {
        return self::METHOD_POST;
    }

    public function path() {
        return "/login";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {

        try {

            $session = new Session();
            $controller = new LoginController( $storage = (new Factory(new MySQLPDOConfiguration()))->userStorage());

            if($session->loggedIn()) {
                $response->redirect('/');
                return;
            }

            //Verify the request
            $controller->verifyRequest($request);

            $controller->login($request);
            $response->redirect('/');

        } catch (InvalidUsername $e) {

            //If InvalidUsername, show error
            $response->redirect((new LoginPage())->makeURL(array(
                LoginPage::PARAM_ERROR => LoginPage::ERROR_INVALID_USERNAME
            )));
            return;

        } catch (InvalidPassword $e) {

            //If InvalidPassword, show error and autofill username
            $response->redirect((new LoginPage())->makeURL(array(
                LoginPage::PARAM_ERROR => LoginPage::ERROR_INVALID_PASSWORD,
                LoginPage::PARAM_FILL_USERNAME => $request->param(LoginController::REQUEST_PARAM_USERNAME)
            )));
            return;

        } catch (InvalidCombination $e) {

            $response->redirect((new LoginPage())->makeURL(array(
                LoginPage::PARAM_ERROR => LoginPage::ERROR_INVALID_COMBINATION,
                LoginPage::PARAM_FILL_USERNAME => $request->param(LoginController::REQUEST_PARAM_USERNAME)
            )));
            return;

        } catch (UserDoesNotExist $e) {

            $response->redirect((new LoginPage())->makeURL(array(
                LoginPage::PARAM_ERROR => LoginPage::ERROR_INVALID_COMBINATION,
                LoginPage::PARAM_FILL_USERNAME => $request->param(LoginController::REQUEST_PARAM_USERNAME)
            )));
            return;

        } catch (\Exception $e) {

            $response->render('error/500');
            return;

        }

    }

}