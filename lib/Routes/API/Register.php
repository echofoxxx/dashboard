<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 8/5/15
 * Time: 4:47 PM
 */

namespace Dashboard\Routes\API;


use Dashboard\Accounts\Session;
use Dashboard\Controllers\RegistrationController;
use Dashboard\Exceptions\Controllers\RegistrationController\EmailInUse;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidConfirmPassword;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidPassword;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidUsername;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidEmail;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidName;
use Dashboard\Exceptions\Controllers\RegistrationController\PasswordsDoNotMatch;
use Dashboard\Exceptions\Controllers\RegistrationController\UsernameInUse;
use Dashboard\Http\HttpRequest;
use Dashboard\Http\HttpResponse;
use Dashboard\Route;

use Dashboard\Routes\Login as LoginPage;
use Dashboard\Routes\Register as RegisterPage;
use Dashboard\Storage\MySQL\Factory;
use Dashboard\Storage\MySQL\MySQLPDOConfiguration;

class Register extends Route {

    public function method() {
        return self::METHOD_POST;
    }

    public function path() {
        return "/register";
    }

    public function onRequest(HttpRequest $request, HttpResponse $response, $app) {

        try {

            $session = new Session();
            $controller = new RegistrationController( $storage = (new Factory(new MySQLPDOConfiguration()))->userStorage());

            if ($session->loggedIn()) {
                $response->redirect('/');
                return;
            }

            $controller->verifyRequest($request);
            $controller->register($request);

            $response->redirect((new LoginPage())->path());

        } catch (InvalidEmail $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_INVALID_EMAIL,
                RegisterPage::PARAM_FILL_USERNAME => $request->param(RegistrationController::REQUEST_PARAM_USERNAME),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (InvalidUsername $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_INVALID_USERNAME,
                RegisterPage::PARAM_FILL_EMAIL => $request->param(RegistrationController::REQUEST_PARAM_EMAIL),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (InvalidName $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_INVALID_NAME,
                RegisterPage::PARAM_FILL_EMAIL => $request->param(RegistrationController::REQUEST_PARAM_EMAIL),
                RegisterPage::PARAM_FILL_USERNAME => $request->param(RegistrationController::REQUEST_PARAM_USERNAME)
            )));

        } catch (InvalidPassword $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_INVALID_PASSWORD,
                RegisterPage::PARAM_FILL_EMAIL => $request->param(RegistrationController::REQUEST_PARAM_EMAIL),
                RegisterPage::PARAM_FILL_USERNAME => $request->param(RegistrationController::REQUEST_PARAM_USERNAME),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (InvalidConfirmPassword $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_INVALID_CONFIRM_PASSWORD,
                RegisterPage::PARAM_FILL_EMAIL => $request->param(RegistrationController::REQUEST_PARAM_EMAIL),
                RegisterPage::PARAM_FILL_USERNAME => $request->param(RegistrationController::REQUEST_PARAM_USERNAME),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (EmailInUse $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_EMAIL_IN_USE,
                RegisterPage::PARAM_FILL_USERNAME => $request->param(RegistrationController::REQUEST_PARAM_USERNAME),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (UsernameInUse $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_INVALID_NAME,
                RegisterPage::PARAM_FILL_EMAIL => $request->param(RegistrationController::REQUEST_PARAM_EMAIL),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (PasswordsDoNotMatch $e) {

            $response->redirect((new RegisterPage())->makeURL(array(
                RegisterPage::PARAM_ERROR => RegisterPage::ERROR_PASSWORDS_DO_NOT_MATCH,
                RegisterPage::PARAM_FILL_EMAIL => $request->param(RegistrationController::REQUEST_PARAM_EMAIL),
                RegisterPage::PARAM_FILL_USERNAME => $request->param(RegistrationController::REQUEST_PARAM_USERNAME),
                RegisterPage::PARAM_FILL_NAME => $request->param(RegistrationController::REQUEST_PARAM_NAME)
            )));

        } catch (\Exception $e) {
            $response->render('error/500');
        }

    }

}