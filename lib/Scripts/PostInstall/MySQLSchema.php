<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 30/4/15
 * Time: 3:52 PM
 */

namespace Dashboard\Scripts\PostInstall;

use Composer\IO\IOInterface;
use Composer\Script\Event;

use Dashboard\Storage\MySQL\Factory;
use Dashboard\Storage\PDOConfigurationFromINI;

class MySQLSchema {

    /**
     * @var IOInterface
     */
    protected static $io;

    /**
     * @var \PDO
     */
    protected static $pdo;

    /**
     * @var PDOConfigurationFromINI
     */
    private static $config;

    public static function getConfiguration() {
        return new PDOConfigurationFromINI(__DIR__."/../../../config.ini");
    }

    public static function getSchema() {
        return json_decode(file_get_contents(__DIR__."/../../../schema.json"), true)["mysql"][self::$config->get('mysql')['schema']];
    }

    private static function createTables($tables, $force = false) {
        foreach ($tables as $table) {
            self::createTable($table, $force);
        }
    }

    private static function createTable($table, $force = false) {
        $sql = self::createTableSQL($table, $force);
        self::$io->write($sql);
        self::$pdo->exec($sql);
    }

    private static function createTableSQL($table, $force = false) {
        $sql = $force ? "DROP TABLE IF EXISTS ".$table["name"]."; " : "";
        $sql .= "CREATE TABLE " . (!$force ? "IF NOT EXISTS " : "");
        $sql .= $table["name"];
        $sql .= join(" ", array("(" , join(",", $table["columns"]) , ")"));
        $sql .= array_key_exists("engine", $table) ? " ENGINE = ".$table["engine"] : "";
        $sql .= ";";
        return $sql;
    }

    public static function postInstall(Event $event) {
        self::$io = $event->getIO();
        $config = self::$config = self::getConfiguration();
        self::$pdo = (new Factory($config))->pdo;
        self::$io->write("Database : ".$config->get('mysql')['dbname']);
        self::$io->write("Schame : ".$config->get('mysql')['schema']);
        $force = self::$io->askConfirmation("Do you want to clear the database before initializing a new one? (yes/no) [no] ", false);
        self::createTables(self::getSchema(), $force);
    }

    public static function runForTests(Event $event) {
        self::$io = $event->getIO();
        if(!self::$io->askConfirmation("Do you want to run the setup for unit testing? (yes/no) [yes] ", true)) return;
        $config = self::$config = self::getConfiguration();
        $mysql = $config->get('mysql');
        $mysql['dbname'] = "dashboard-test";
        $config->set('mysql', $mysql);
        self::$config = $config;
        self::$pdo = null;
        self::$pdo = (new Factory(self::$config))->pdo;
        self::$io->write("Database : ".$config->get('mysql')['dbname']);
        self::$io->write("Schame : ".$config->get('mysql')['schema']);
        $force = self::$io->askConfirmation("Do you want to clear the database before initializing a new one? (yes/no) [yes] ", true);
        self::createTables(self::getSchema(), $force);
    }

}