<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 8/5/15
 * Time: 12:44 AM
 */

namespace Dashboard\Storage\MySQL;


use Dashboard\Storage\PDOConfigurationFromINI;

class MySQLPDOConfiguration extends PDOConfigurationFromINI {

    public function __construct() {
        parent::__construct(__DIR__."/../../../config.ini");
    }

}