<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 6:58 PM
 */

namespace Dashboard\Storage\MySQL;


use Dashboard\Storage\Accounts\UserStorage;
use Dashboard\Storage\PDOConfiguration;

class Factory {

    public $pdo = null;

    public function __construct(PDOConfiguration $config) {
        $this->init($config);
    }

    private function init(PDOConfiguration $config) {
        $this->pdo = new \PDO($this->dsn($config), $config->get('mysql')['username'], $config->get('mysql')['password']);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }

    private function dsn(PDOConfiguration $config) {
        return "mysql:host=".
        $config->get('mysql')['host'].
        ((!empty($config->get('mysql')['port'])) ? (';port='.$config->get('mysql')['port']) : '').
        ';dbname='.$config->get('mysql')['dbname'];
    }

    public function userStorage() {
        return new UserStorage($this->pdo);
    }

}