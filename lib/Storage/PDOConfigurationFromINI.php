<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 6:43 PM
 */

namespace Dashboard\Storage;

/**
 * @see http://php.net/manual/en/class.pdo.php#89019
 * Class PDOConfigurationFromINI
 * @package Dashboard\Storage
 */
class PDOConfigurationFromINI extends PDOConfiguration {

    private $filepath = "pdo.ini";

    public function __construct($filepath) {
        $this->filepath = $filepath;
        parent::__construct();
    }

    protected function read() {
        return ($config = parse_ini_file($this->filepath, true)) ? $config : false;
    }

}