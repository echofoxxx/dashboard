<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 6:14 PM
 */

namespace Dashboard\Storage\Accounts;


use Dashboard\Accounts\User\IdentityCard;
use Dashboard\Accounts\User\Profile;
use Dashboard\Accounts\User\RegistrationForm;
use Dashboard\Accounts\UserStorage\UserStorageInterface;
use Dashboard\Exceptions\Accounts\User\UserDoesNotExist;
use Dashboard\Exceptions\DatabaseError;

class UserStorage implements UserStorageInterface {

    const TABLE = "users";

    private $PDO = null;

    public function __construct(\PDO $PDO) {
        $this->PDO = $PDO;
    }
    public function create(RegistrationForm $form) {
        try {
            $stmt = $this->PDO->prepare(sprintf("INSERT INTO %s (username, email, password, name) VALUES (:username, :email, :password, :name)", self::TABLE));
            $stmt->execute($form->gets(array('username', 'email', 'password', 'name')));
            return (int) $this->PDO->lastInsertId();
        } catch (\Exception $e) {
            fwrite(STDERR, $e->getMessage());
            throw new DatabaseError;
        }
    }

    public function read(IdentityCard $card) {
        try {
            $stmt = $this->PDO->prepare(sprintf("SELECT * FROM %s WHERE %s=:%s AND deleted=0", self::TABLE, $card->key(), $card->key()));
            $stmt->execute(array($card->key()=>$card->value()));
        } catch (\Exception $e) {
            throw new DatabaseError;
        }
        if(!$data = $stmt->fetch()) {
            throw new UserDoesNotExist;
        }
        $data['id'] = (int) $data['id'];
        return $data;
    }

    public function update(IdentityCard $card, Profile $profile) {

        $dataToUpdate = $profile->gets();

        //If no data to update, just return
        if(!count($dataToUpdate)) return true;

        $query = sprintf("UPDATE %s SET", self::TABLE);

        //Build the query
        foreach ($dataToUpdate as $key=>$value) {
            $query .= " ".$key."=:".$key;
        }

        $query .= sprintf(" WHERE %s=:%s AND deleted=0", $card->key(), $card->key());

        $dataToUpdate[$card->key()] = $card->value();

        try {
            $stmt = $this->PDO->prepare($query);
            $stmt->execute($dataToUpdate);
        } catch (\Exception $e) {
            print_r($e->getMessage());
            throw new DatabaseError;
        }

        return true;

    }

    public function delete(IdentityCard $card) {
        try {
            $stmt = $this->PDO->prepare(sprintf("UPDATE %s SET deleted=1 WHERE %s=:%s", self::TABLE, $card->key(), $card->key()));
            $stmt->execute(array($card->key()=>$card->value()));
        } catch (\Exception $e) {
            throw new DatabaseError;
        }
    }

}