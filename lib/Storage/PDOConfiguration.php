<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 6:40 PM
 */

namespace Dashboard\Storage;


use Dashboard\Util\KeyValueStorage;

abstract class PDOConfiguration extends KeyValueStorage {

    public function __construct() {

        if(!$config = $this->read()) {
            throw new \Exception('Unable to read configuration');
        }

        parent::__construct($config);

    }

    /**
     * Read the configuration
     * @return array|false
     */
    abstract protected function read();

}