<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 4:14 PM
 */

namespace Dashboard\Controllers;


use Dashboard\Accounts\User;
use Dashboard\Accounts\User\IdentityCard;
use Dashboard\Accounts\User\RegistrationForm;
use Dashboard\Accounts\UserStorage\UserStorageInterface;
use Dashboard\Exceptions\Accounts\User\UserDoesNotExist;
use Dashboard\Exceptions\Controllers\RegistrationController\EmailInUse;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidConfirmPassword;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidEmail;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidName;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidPassword;
use Dashboard\Exceptions\Controllers\RegistrationController\InvalidUsername;
use Dashboard\Exceptions\Controllers\RegistrationController\PasswordsDoNotMatch;
use Dashboard\Exceptions\Controllers\RegistrationController\UsernameInUse;
use Dashboard\Http\HttpRequest;

class RegistrationController {

    public static $isOpen = true;

    const REQUEST_PARAM_EMAIL = "email";
    const REQUEST_PARAM_USERNAME = "username";
    const REQUEST_PARAM_NAME = "name";
    const REQUEST_PARAM_PASSWORD = "password";
    const REQUEST_PARAM_CONFIRM_PASSWORD = "confirm-password";

    const USERNAME_MIN_LENGTH = 6;
    const USERNAME_MAX_LENGTH = 24;

    const PASSWORD_MIN_LENGTH = 8;

    /**
     * @var UserStorageInterface
     */
    private $storage;

    public function __construct(UserStorageInterface $userStorage) {
        $this->storage = $userStorage;
    }

    public function verifyRequest(HttpRequest $request) {
        if(!$this->verifyEmail($request)) throw new InvalidEmail;
        if(!$this->verifyUsername($request)) throw new InvalidUsername;
        if(!$this->verifyName($request)) throw new InvalidName;
        if(!$this->verifyPassword($request)) throw new InvalidPassword;
        if(!$this->verifyConfirmPassword($request)) throw new InvalidConfirmPassword;
        if(!$this->verifyEmailAvailability($request)) throw new EmailInUse;
        if(!$this->verifyUsernameAvailability($request)) throw new UsernameInUse;
        if($request->param(self::REQUEST_PARAM_PASSWORD) !== $request->param(self::REQUEST_PARAM_CONFIRM_PASSWORD)) throw new PasswordsDoNotMatch;
        return true;
    }

    public function verifyEmail(HttpRequest $request) {
        return ($email = $request->param(self::REQUEST_PARAM_EMAIL)) && filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function verifyUsername(HttpRequest $request) {
        return ($username = $request->param(self::REQUEST_PARAM_USERNAME)) && (strlen($username) >= self::USERNAME_MIN_LENGTH) && (strlen($username) <= self::USERNAME_MAX_LENGTH);
    }

    public function verifyName(HttpRequest $request) {
        return ($name = $request->param(self::REQUEST_PARAM_NAME)) && strlen($name);
    }

    public function verifyPassword(HttpRequest $request) {
        return ($password = $request->param(self::REQUEST_PARAM_PASSWORD)) && strlen($password) >= self::PASSWORD_MIN_LENGTH;
    }

    public function verifyConfirmPassword(HttpRequest $request) {
        return ($password = $request->param(self::REQUEST_PARAM_CONFIRM_PASSWORD)) && strlen($password) >= self::PASSWORD_MIN_LENGTH;
    }

    public function verifyEmailAvailability(HttpRequest $request) {
        try {
            $this->storage->read(new IdentityCard(array(
                'email' => $request->param(self::REQUEST_PARAM_EMAIL)
            )));
        } catch (UserDoesNotExist $e) {
            return true;
        }
        return false;
    }

    public function verifyUsernameAvailability(HttpRequest $request) {
        try {
            $this->storage->read(new IdentityCard(array(
                'username' => $request->param(self::REQUEST_PARAM_USERNAME)
            )));
        } catch (UserDoesNotExist $e) {
            return true;
        }
        return false;
    }

    public function register(HttpRequest $request) {
        $form = new RegistrationForm(array(
            'email' => $request->param(self::REQUEST_PARAM_EMAIL),
            'username' => $request->param(self::REQUEST_PARAM_USERNAME),
            'name' => $request->param(self::REQUEST_PARAM_NAME),
            'password' => $request->param(self::REQUEST_PARAM_PASSWORD)
        ));
        return User::create($form, $this->storage);
    }

}