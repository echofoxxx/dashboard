<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 8/5/15
 * Time: 12:33 PM
 */

namespace Dashboard\Controllers;


use Dashboard\Accounts\Session;
use Dashboard\Accounts\User;
use Dashboard\Accounts\User\IdentityCard;
use Dashboard\Accounts\UserStorage\UserStorageInterface;
use Dashboard\Exceptions\Controllers\LoginController\InvalidCombination;
use Dashboard\Exceptions\Controllers\LoginController\InvalidPassword;
use Dashboard\Exceptions\Controllers\LoginController\InvalidUsername;
use Dashboard\Http\HttpRequest;

class LoginController {

    const REQUEST_PARAM_USERNAME = "username";
    const REQUEST_PARAM_PASSWORD = "password";

    private $storage;

    public function __construct(UserStorageInterface $userStorage) {
        $this->storage = $userStorage;
    }

    /**
     * Verify a HttpRequest for login
     * @param HttpRequest $request
     * @return bool
     * @throws InvalidCombination When wrong password is provided
     * @throws InvalidPassword When invalid password is provided
     * @throws InvalidUsername When invalid username is provided
     */
    public function verifyRequest(HttpRequest $request) {
        if(!$this->checkUsername($request)) throw new InvalidUsername;
        if(!$this->checkPassword($request)) throw new InvalidPassword;
        if(!$this->verifyPassword($request)) throw new InvalidCombination;
        return true;
    }

    /**
     * Perform login
     * @param HttpRequest $request
     */
    public function login(HttpRequest $request) {
        $user = new User($this->getIdentityCardFromRequest($request), $this->storage);
        $session = new Session();
        $session->logIn($user);
    }

    /**
     * Match the password against the correct one
     * @param HttpRequest $request
     * @return bool
     */
    private function verifyPassword(HttpRequest $request) {
        $user = new User($this->getIdentityCardFromRequest($request), $this->storage);
        return $user->verify($request->param(self::REQUEST_PARAM_PASSWORD));
    }

    /**
     * Get the requested user's identity card from the request
     * @param HttpRequest $request
     * @return IdentityCard
     */
    private function getIdentityCardFromRequest(HttpRequest $request) {
        return new IdentityCard(array(
            'username' => $request->param(self::REQUEST_PARAM_USERNAME)
        ));
    }

    /**
     * Check the validity of the username provided in a request
     * @param HttpRequest $request
     * @return bool
     */
    private function checkUsername(HttpRequest $request) {
        return ($username = $request->param(self::REQUEST_PARAM_USERNAME)) && strlen($username);
    }

    /**
     * Check the validity of the password provided in a request
     * @param HttpRequest $request
     * @return bool
     */
    private function checkPassword(HttpRequest $request) {
        return ($password = $request->param(self::REQUEST_PARAM_PASSWORD)) && strlen($password);
    }

}