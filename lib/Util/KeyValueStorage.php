<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 29/4/15
 * Time: 5:19 PM
 */

namespace Dashboard\Util;

/**
 * Stores data in key-value pairs
 * Class KeyValueStorage
 * @package Dashboard\Util
 */
class KeyValueStorage {

    private $data = array();

    /**
     * @param array [$default] An array containing default pairs
     */
    public function __construct(array $default = array()) {
        $this->data = $default;
    }

    /**
     * Check if a pair with the provided key is stored
     * @param string $key
     * @return bool
     */
    public function has($key) {
        return array_key_exists($key, $this->data);
    }

    /**
     * Get the value mapped to the provided key
     * @param string $key
     * @return mixed
     */
    public function get($key) {
        return $this->has($key) ? $this->data[$key] : null;
    }

    /**
     * Get multiple pairs by keys
     * @param array $keys
     * @return array
     */
    public function gets(array $keys = array()) {
        if(!count($keys)) return $this->data;
        $result = array();
        foreach ($keys as $key) {
            $result[$key] = $this->get($key);
        }
        return $result;
    }

    /**
     * Set the value of a key
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function set($key, $value) {
        $this->data[$key] = $value;
        return $this;
    }

    /**
     * Set the values of multiple keys
     * @param array $pairs
     * @return $this
     */
    public function sets(array $pairs = array()) {
        $this->data = array_merge($this->data, $pairs);
        return $this;
    }

    /**
     * Remove the value mapped to the provided key, if stored
     * @param string $key
     * @return $this
     */
    public function delete($key) {
        unset($this->data[$key]);
        return $this;
    }

    /**
     * Remove multiple stored pairs
     * @param array $keys
     * @return $this
     */
    public function deletes(array $keys = array()) {
        foreach ($keys as $key) {
            $this->delete($key);
        }
        return $this;
    }

}