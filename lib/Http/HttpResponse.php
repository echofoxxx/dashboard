<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 11:54 AM
 */

namespace Dashboard\Http;

use Smarty;

class HttpResponse {

    /**
     * @var Smarty
     */
    private $smarty;

    public function __construct() {
        $this->initializeSmarty();
    }

    private function initializeSmarty() {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir(__DIR__."/../../smarty/templates");
        $this->smarty->setCompileDir(__DIR__."/../../smarty/compiled");
        $this->smarty->setCacheDir(__DIR__."/../../smarty/cache");
        $this->smarty->setConfigDir(__DIR__."/../../smarty/configs");
    }

    public function render($view = "index", $params = array()) {
        $this->smarty->clearAllAssign();
        $this->smarty->assign($params);
        echo $this->smarty->fetch($view.".tpl");
    }

    public function redirect($url) {
        ob_start();
        header('Location: '.$url);
        ob_end_flush();
        die();
    }

}