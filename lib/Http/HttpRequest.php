<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 7/5/15
 * Time: 1:03 PM
 */

namespace Dashboard\Http;


class HttpRequest {

    public function getParams() {
        return $_GET;
    }

    public function postParams() {
        return $_POST;
    }

    public function params() {
        return array_merge($this->getParams(), $this->postParams());
    }

    public function param($key, $default = null) {
        return array_key_exists($key, $arr = $this->params()) ? $arr[$key] : $default;
    }

}