<?php
/**
 * Created by PhpStorm.
 * User: echofoxxx
 * Date: 30/4/15
 * Time: 10:54 AM
 */

namespace Dashboard\Exceptions\Accounts\User;


class UserDoesNotExist extends UserException {
    
}